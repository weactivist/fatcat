<?hh // strict
	use \core\Auth;

	class Config {
		/**
		 * Database credentials.
		 */
		public Map $database = Map {
			"host" => "127.0.0.1",
			"database" => "",
			"user" => "",
			"password" => ""
		};

		/**
	 	 * Base paths of controllers and models. Change path if models or controllers are located somewhere else.
		 */
		public Map $paths = Map {
			"controllers" => __DIR__."/controller/",
			"models" => __DIR__."/model/"
		};

		/**
		 * Set if app should be in debug mode outputing debug information.
		 */
		public bool $debug = false;

		/**
		 * Random key of at least 10 characters.
		 */
		public string $randomKey = "";

		/**
		 * Define multiple domains for applications to allow access to. i.e "http://www.example.com"
		 */
		public Set $allowOrigin = Set {};

		public Map $routes;

		public function __construct() {
			$this->routes = Map {
				"/" => function( Auth $auth ) {
					return ""; // Return controller for root here. i.e "IndexController::Index"
				}
			};
		}
	}
