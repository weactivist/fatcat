<?hh // strict
	namespace core;

	class Str {
		public static function classToAttribute( string $className ): string {
			$self = new self( $className );
			$self->toAttribute();

			return $self->get();
		}

		public static function classToColumn( string $className ): string {
			return self::classToAttribute( $className )."_id";
		}

		public static function random( int $length ): string {
			$characters = "abcdefghijklmnopqrstuvwxyz";

			$string = "";

			for( $i = 0; $i < $length; $i++ ) {
				$string .= $characters[rand( 0, mb_strlen( $characters ) )];
			}

			return $string;
		}

		private string $string;

		public function __construct( string $string ): void {
			$this->string = $string;
		}

		public function get(): string {
			return $this->string;
		}

		public function toAttribute(): void {
			preg_match( "/([a-z]+)Model/i", $this->string, $matches );

			if( isset( $matches[1] ) ) {
				$this->string = $matches[1];
			}

			$this->toLower();
		}

		public function toLower(): void {
			$this->string = strtolower( $this->string );
		}

		public function capitalize(): void {
			$this->string = ucwords( $this->string );
		}

		public function __toString(): void {
			echo $this->string;
		}
	}