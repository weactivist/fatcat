<?hh // strict
	use \core\Route;
	use \core\Controller;
	use \core\db\Stats;

	class App {
		private \Config $config;

		public function __construct(): void {
			set_exception_handler( array( $this, "ExceptionHandler" ) );
			set_error_handler( array( $this, "ErrorHandler" ) );
			set_include_path( __DIR__ );
			spl_autoload_extensions('.hh');
			spl_autoload_register( array( $this, "AutoLoader" ) );

			$this->config = new \Config();

			http_response_code( 200 );
		}

		public function AutoLoader( string $classname ): void {
			$filename = str_replace( "\\", "/", $classname ).".hh";

			if( strpos( $filename, "controller/" ) === 0 ) {
				$path = $this->config->paths->get( "controllers" ).$filename;
			}
			elseif( strpos( $filename, "model/" ) === 0 ) {
				$path = $this->config->paths->get( "models" ).$filename;
			}
			else {
				$path = __DIR__."/../".$filename;
			}

			require_once( $path );
		}

		public function ExceptionHandler( Exception $ex ) {
			$this->Response( Map{ "error" => $ex->getMessage() } );
		}

		public function ErrorHandler( int $errno , string $errstr, string $errfile, int $errline, array $errcontext ): void {
			http_response_code( 500 );

			switch( $errno )
			{
				case E_NOTICE:
				case E_USER_NOTICE: 		$message = "NOTICE: $errstr at $errfile:$errline"; break;
				case E_WARNING:
				case E_USER_WARNING:		$message = "WARNING: $errstr at $errfile:$errline"; break;
				case E_RECOVERABLE_ERROR:
				case E_USER_ERROR:			$message = "ERROR: $errstr at $errfile:$errline"; exit(1); break;
				default:					$message = "Unknown error: $errstr at $errfile:$errline";
			}

			$this->Response( Map{ "error" => $message } );
			exit();
		}

		public function Response( Collection $data ): void {
			header( "Content-Type: application/json" );
			header( "Access-Control-Allow-Methods: GET, POST, PUT, DELETE" );
			header( "Access-Control-Allow-Credentials: true" );

			if( isset( $_SERVER["HTTP_ORIGIN"] ) && $this->config->allowOrigin->contains( $_SERVER["HTTP_ORIGIN"] ) ) {
				header( "Access-Control-Allow-Origin: ".$_SERVER["HTTP_ORIGIN"] );
			}

			$response = array(
				"status" => http_response_code(),
				"data" => $this->BuildArrayTree( $data->toArray() )
			);

			if( $this->config->debug ) {
				$response["debug"] = array(
					"database" => array(
						"count" => Stats::instance()->getQueries()->count(),
						"queries" => Stats::instance()->getQueries()->toArray()
					)
				);
			}

			echo json_encode( $response );
		}

		public function Run(): void {
			$route = new Route();

			$reflectionClass = new ReflectionClass( $route->GetControllerName() );
			$instance = $reflectionClass->newInstance();

			$reflectionMethod = new ReflectionMethod( $route->GetControllerName(), $route->GetControllerMethod() );

			$map = $reflectionMethod->invokeArgs( $instance, $route->GetArgs() );

			$this->Response( $map );
		}

		private function BuildArrayTree( array $array ) {
			foreach( $array AS $key => $value )
			{
				if( is_object( $value ) )
					$array[$key] = $this->BuildArrayTree( $value->toArray() );
				else
					$array[$key] = $value;
			}

			return $array;
		}
	}
