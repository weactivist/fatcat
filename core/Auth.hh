<?hh // strict
	namespace core;

	use model\SessionModel;

	class Auth {
		static Auth $instance;

		public static function instance(): Auth {
			if( !( self::$instance instanceof Auth ) ) {
				self::$instance = new self();
				self::$instance->initSession();
			}

			return self::$instance;
		}

		public static function CreateSession( int $userId ): string {
			// Fetch server vars
			$server = Map::fromArray( $_SERVER );

			// Fetch users IP and current timestamp
			$ip = preg_replace( '/[^0-9]/', "", $server->get( "REMOTE_ADDR" ) );
			$timestamp = time();

			// Create session row in database
			$session = new SessionModel();
			$session->ip = $ip;
			$session->user_id = $userId;
			$session->timestamp = $timestamp;
			$sessionId = $session->Save();

			// Set cookie from vars
			setcookie( "auth", $session->getClientKey(), null, "/");

			return $session->getClientKey();
		}

		public static function UserIsLoggedIn() {
			return self::instance()->isLoggedIn();
		}

		private string $clientKey = "";
		private int $userId = 0;

		private function __construct() {}

		public function isLoggedIn(): bool {
			return $this->userId > 0;
		}

		public function destroySession() {
			$session = $this->getSession();
			$session->delete();

			setcookie( "auth", null, -1, "/");

			self::$instance = new self();
		}

		public function requireLoggedIn() {
			if( !$this->isLoggedIn() ) {
				Exception::Unauthorized();
			}
		}

		public function getClientKey(): string {
			return $this->clientKey;
		}

		public function getUserId(): int {
			return $this->userId;
		}

		private function getSession(): SessionModel {
			$cookie = Map::fromArray( $_COOKIE );

			if( !$cookie->get( "auth" ) ) {
				return new SessionModel();
			}

			$auth = explode( "-", base64_decode( $cookie->get( "auth" ) ) );

			list( $sessionId, $ip, $timestamp, $hash ) = $auth;

			$sessionId 	= intval( $sessionId );
			$ip 		= intval( $ip );
			$timestamp 	= intval( $timestamp );

			$config = new \Config();

			// Unique string does not match. Someone tried to create their own cookie.
			if( $hash != sha1( $sessionId."-".$ip."-".$timestamp."-".$config->randomKey ) ) {
				return new SessionModel();
			}

			$session = SessionModel::Get( $sessionId );

			if( $session->ip === $ip && $session->timestamp === $timestamp ) {
				return $session;
			}

			return new SessionModel();
		}

		private function initSession(): void {
			$session = $this->getSession();

			$this->userId = $session->user_id;
			$this->clientKey = $session->getClientKey();
		}
	}