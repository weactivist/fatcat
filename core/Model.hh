<?hh // strict
	namespace core;

	use \core\db\Insert;
	use \core\db\Select;
	use \core\db\Update;
	use \core\db\Delete;
	use \core\db\Where;
	use \core\Utility;
	use \core\Exception;
	use \core\Query;

	class Model {
		final public static function Get( int $id ): Model {
			$class = get_called_class();

			$model = new $class();
			return $model->Find( $id );
		}

		public int $id = 0;
		public string $table;

		private int $limit = 0;
		private Map $models = Map{};
		private Where $whereClause;
		private string $join = "";
		private Vector $where = Vector{};
		private Set $reservedProperties = Set{ "table" };
		private Query $query;

		/**
		 * Override construct to load relations.
		 */
		public function __construct() {
			$this->query = new Query( $this );
		}

		/**
		 * Override this method for content to be validated before saving model.
		 * This method will execute before save.
		 * @throws Exception
		 * @return void
		 */
		protected function validate() {}

		final public function All(): Vector {
			return $this->PrepareData( $this->query->select->query() );
		}

		final public function Exists(): bool {
			return !$this->All()->isEmpty();
		}

		final public function Find( int $id ): Model {
			$this->query->select->where( "AND", "id", "=", $id );
			$this->query->select->limit( 1 );

			$models = $this->All();

			if( $models->isEmpty() ) {
				Exception::NotFound();
			}

			return $models->get( 0 );
		}

		final public function belongsTo( string $modelName ): void {
			if( $this->id <= 0 ) {
				return;
			}

			$name = Str::classToAttribute( $modelName );
			$this->models->set( $name, $modelName );

			$this->$name = function( $closure ) {
				$reflection = new \ReflectionClass( $this->models->get( $closure ) );
				$column = Str::classToColumn( $closure );

				$method = $reflection->getMethod( "Where" );
				$class = $method->invoke( $reflection->newInstance(), "id", "=", $this->$column );

				if( ( $model = $class->All()->get( 0 ) ) !== NULL ) {
					return $model;
				}

				return NULL;
			};
		}

		final public function hasMany( string $modelName ): void {
			$name = Str::classToAttribute( $modelName )."s";
			$this->models->set( $name, $modelName );

			$this->$name = function( $closure ) {
				if( $this->id <= 0 ) {
					return Vector{};
				}

				$reflection = new \ReflectionClass( $this->models->get( $closure ) );

				$method = $reflection->getMethod( "Where" );
				$class = $method->invoke( $reflection->newInstance(), Str::classToColumn( get_called_class() ), "=", $this->id );

				$models = $class->All();

				if( !$models->isEmpty() ) {
					return $models;
				}

				return Vector{};
			};
		}

		final public function hasManyThrough( string $modelName, string $tableName, Where $where = NULL ): void {
			if( $this->id <= 0 ) {
				return;
			}

			$name = Str::classToAttribute( $modelName )."s";
			$this->models->set( $name, $modelName );
			$this->join = $tableName;

			if( $where instanceof Where ) {
				$this->whereClause = $where;
			}

			$this->$name = function( $closure ) {
				$reflection = new \ReflectionClass( $this->models->get( $closure ) );

				$class = $reflection->newInstance();

				$class->query->select->join( $this->join, "id", Str::classToColumn( $reflection->getName() ) );

				if( $this->whereClause instanceof Where ) {
					$class->query->select->whereClause( $this->whereClause );
				}

				$class->query->select->where( "AND", Str::classToColumn( get_called_class() ), "=", $this->id );

				$models = $class->All();

				if( !$models->isEmpty() ) {
					return $models;
				}

				return Vector{};
			};
		}

		final public function hasOne( string $modelName ): void {
			if( $this->id <= 0 ) {
				return;
			}

			$name = Str::classToAttribute( $modelName );
			$this->models->set( $name, $modelName );

			$this->$name = function( $closure ) {
				$reflection = new \ReflectionClass( $this->models->get( $closure ) );

				$method = $reflection->getMethod( "Where" );
				$class = $method->invoke( $reflection->newInstance(), Str::classToColumn( get_called_class() ), "=", $this->id );

				if( ( $model = $class->All()->get( 0 ) ) !== NULL ) {
					return $model;
				}

				return NULL;
			};
		}

		final public function Limit( int $count ): Model {
			$this->query->select->limit( $count );
			return $this;
		}

		final public function Save(): int {
			$this->validate();

			$reflection = new \ReflectionClass( $this );

			$properties = array_filter( $reflection->getProperties(), function( $property ) use ( $reflection )
			{
				return (
					$property->getDeclaringClass()->getName() == $reflection->getName() &&
					!$this->reservedProperties->contains( $property->getName() ) &&
					isset( $property->info["default"] )
				);
			} );

			$fields = array();

			foreach( $properties AS $property ) {
				$fields[$property->getName()] = ( $property->getValue( $this ) ) ? $property->getValue( $this ) : $property->info["defaultValue"];
			}

			if( $this->id > 0 ) {
				$statement = new Update( $this->table );
				$statement->where( "AND", "id", "=", $this->id );
			}
			else {
				$statement = new Insert( $this->table );
			}

			foreach( $fields AS $field => $value ) {
				$statement->field( $field, $value );
			}

			$statement->query();

			if( $this->id <= 0 ) {
				$this->id = $statement->lastInsertId();
			}

			return intval( $this->id );
		}

		final public function delete(): void {
			$statement = new Delete( $this->table );
			$statement->where( "AND", "id", "=", $this->id );
			$statement->query();
		}

		final public function Where( string $column, string $operator, mixed $value ): Model {
			$this->query->select->where( "AND", $column, $operator, $value );
			return $this;
		}

		final public function AndWhere( string $column, string $operator, mixed $value ): Model {
			$this->query->select->where( "AND", $column, $operator, $value );
			return $this;
		}

		final public function orderBy( string $column, bool $desc = false ): Model {
			$this->query->select->orderBy( $column, $desc );
			return $this;
		}

		final public function __call( string $closure, array $args ) {
			return call_user_func_array( $this->{$closure}->bindTo( $this ), array( $closure ) );
		}

		final public function __toString() {
			return call_user_func( $this->{"__toString"}->bindTo( $this ) );
		}

		final private function PrepareData( \PDOStatement $statement ): Vector {
			$data = Utility::ArrayToVector( $statement->fetchAll( \PDO::FETCH_CLASS, get_called_class() ) );

			$reflection = new \ReflectionClass( $this );

			$properties = array_filter( $reflection->getProperties(), function( $property ) use ( $reflection ) {
				return ( !$this->reservedProperties->contains( $property->getName() ) );
			} );

			foreach( $data AS $i => $model ) {
				foreach( $properties AS $property ) {
					$name = $property->name;

					switch( $property->info["type"] ) {
						case "HH\int": 		$data->get( $i )->$name = intval( $model->$name ); break;
						case "HH\string": 	$data->get( $i )->$name = htmlspecialchars( $model->$name ); break;
						default: 			$data->get( $i )->$name = $model->$name;
					}
				}
			}

			return $data;
		}
	}
