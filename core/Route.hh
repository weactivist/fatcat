<?hh // strict
	namespace core;

	class Route {
		private Map $args = Map{};
		private Map $routes;
		private string $controllerMethod = "";
		private string $controllerName = "";
		private string $requestUri;

		public function __construct(): void {
			$this->requestUri = explode( "?", $_SERVER["REQUEST_URI"] )[0];

			$config = new \Config();
			$this->routes = $config->routes;

			foreach( $this->routes AS $url => $callback ) {
				if( !preg_match( "/^".str_replace( "/", "\/", $url )."$/", $this->requestUri, $matches ) ) {
					continue;
				}

				$destination = is_string( $callback ) ? $callback : $callback( Auth::instance() );

				$controller = explode( "::", $destination );

				if( count( $controller ) !== 2 ) {
					throw new \Exception( "Routes in config.hh is not configured properly." );
				}

				$this->controllerName = $controller[0];
				$this->controllerMethod = $controller[1];

				foreach( array_slice( $matches, 1 ) AS $key => $value ) {
					switch( $value ) {
						case is_numeric( $value ): $value = intval( $value ); break;
					}

					$this->args->set( $key, $value );
				}
			}

			if( !$this->controllerName )
				Exception::NotFound();
		}

		public function GetArgs(): Map {
			return $this->args;
		}

		public function GetControllerMethod(): string {
			return $this->controllerMethod;
		}

		public function GetControllerName(): string {
			return "\\controller\\".$this->controllerName;
		}
	}