<?hh // strict
	namespace core;

	use \core\db\Select;
	use \core\Exception;

	class Query {
		private Model $model;
		public Select $select;

		public function __construct( Model $model ) {
			$this->model = $model;

			if( !$this->model->table ) {
				Exception::InternalServerError( "Missing table for model." );
			}

			$this->select = new Select( $this->model->table );
		}
	}
