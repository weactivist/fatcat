<?hh // strict
	namespace core;

	class Exception {
		public static function BadRequest( string $message = "400: Bad Request" ): void {
			new self( 400, $message );
		}

		public static function Unauthorized( string $message = "401: Unauthorized" ): void {
			new self( 401, $message );
		}

		public static function Forbidden( string $message = "403: Forbidden" ): void {
			new self( 403, $message );
		}

		public static function Conflict( string $message = "409: Conflict" ): void {
			new self( 409, $message );
		}

		public static function NotFound( string $message = "404: Not Found" ): void {
			new self( 404, $message );
		}

		public static function InternalServerError( string $message = "500: Internal server error" ): void {
			new self( 500, $message );
		}

		public static function ServiceUnavailable( string $message = "503: Service Unavailable" ): void {
			new self( 503, $message );
		}

		public function __construct( int $httpStatus, string $message ): void {
			http_response_code( $httpStatus );
			throw new \Exception( $message );
		}
	}