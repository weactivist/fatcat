<?hh // strict
	namespace core;

	class Utility {
		public static function ArrayToMap( array $array ): Map {
			$map = Map{};

			foreach( $array AS $key => $value ) {
				$map[$key] = is_array( $value ) ? self::ArrayToMap( $value ) : $value;
			}

			return $map;
		}

		public static function ArrayToVector( array $array ): Vector {
			$vector = Vector{};

			foreach( $array AS $key => $value ) {
				$vector->add( is_array( $value ) ? self::ArrayToVector( $value ) : $value );
			}

			return $vector;
		}

		public static function GET(): Map {
			if( count( $_GET ) > 0 ) {
				return new Map( $_GET );
			}

			if( $input = self::getInput() ) {
				return $input;
			}

			return new Map();
		}

		public static function POST(): Map {
			if( count( $_POST ) > 0 ) {
				return new Map( $_POST );
			}

			if( $input = self::getInput() ) {
				return $input;
			}

			return new Map();
		}

		public static function isPOST(): bool {
			return ( $_SERVER["REQUEST_METHOD"] == "POST" );
		}

		private static function getInput(): Map {
			if( $input = file_get_contents('php://input') ) {
				if( $json = json_decode( $input, true ) ) {
					return self::ArrayToMap( $json );
				}
			}

			return new Map();
		}
	}