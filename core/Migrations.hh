<?hh // strict
	namespace core;

	abstract class Migrations {
		abstract public function Run(): void;
		abstract public function Destroy(): void;
	}