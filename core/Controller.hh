<?hh // strict
	namespace core;

	abstract class Controller {
		abstract public function index(): Collection;
	}
