<?hh // strict
    namespace core\db;

    class Stats {
        static $instance;

        public static function instance(): Stats {
            if( !( self::$instance instanceof Stats ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        private Vector $queries = Vector{};

        private function __construct() {}

        public function addQuery( string $query, Vector $values ): void {
            $this->queries->add( Map {
                "query" => $query,
                "values" => $values
            } );
        }

        public function getQueries(): Vector {
            return $this->queries;
        }
    }
