<?hh // strict
	namespace core\db;

	class Insert extends Statement {
		private vector $fields = Vector{};

		public function __construct( string $table ) {
			parent::__construct( "INSERT INTO", $table );
		}

		public function field( string $field, $value ) {
			$this->fields->add( Pair{ $field, $value } );
		}

		protected function buildQuery(): vector {
			$query = Vector{};

			// Statement
			$query->add( $this->statement );

			// Table
			$query->add( $this->table );
			$query->add( "SET" );

			// Fields
			foreach( $this->fields AS $key => $field ) {

				if( $key > 0 )
					$query->add( ", " );

				$query->add( $field->get(0) );
				$query->add( "= ?" );

				$this->values->add( $field->get(1) );
			}

			return $query;
		}
	}