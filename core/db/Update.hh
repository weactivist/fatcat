<?hh // strict
	namespace core\db;

	class Update extends Statement {
		private vector $fields = Vector{};

		public function __construct( string $table ) {
			parent::__construct( "UPDATE", $table );
		}

		public function field( string $field, $value ) {
			$this->fields->add( Pair{ $field, $value } );
		}

		protected function buildQuery(): vector {
			$query = Vector{};

			// Statement
			$query->add( $this->statement );

			// Table
			$query->add( $this->table );
			$query->add( "SET" );

			// Fields
			foreach( $this->fields AS $key => $field ) {
				if( $key > 0 )
					$query->add( ", " );

				$query->add( $field->get(0) );
				$query->add( "= ?" );

				$this->values->add( $field->get(1) );
			}

			// Where
			$query->add( $this->where->query( $this->values ) );

			if( $this->limit > 0 ) {
				$query->add( "LIMIT" );
				$query->add( $this->limit );
			}

			if( $this->offset > 0 ) {
				$query->add( "OFFSET" );
				$query->add( $this->offset );
			}

			return $query;
		}
	}