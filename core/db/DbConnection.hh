<?hh // strict
	namespace core\db;

	use \core\Exception;

	class DbConnection
	{
		static DbConnection $instance;

		public static function instance(): DbConnection {
			if( !( self::$instance instanceof DbConnection ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		private \PDO $pdo;

		public function __construct( string $database = "" ): void
		{
			$config = new \Config();

			if( !$database )
				$database = $config->database->get( "database" );

			try
			{
				$this->pdo = new \PDO( "mysql:host=".$config->database->get( "host" ).";dbname=".$database, $config->database->get( "user" ), $config->database->get( "password" ) );
				$this->pdo->exec("SET NAMES 'utf8';");
			}
			catch( \PDOException $e )
			{
				Exception::ServiceUnavailable( $e->getMessage() );
			}
		}

		public function Resource(): \PDO
		{
			return $this->pdo;
		}
	}