<?hh // strict
	namespace core\db;

	class Delete extends Statement {
		public function __construct( string $table ) {
			parent::__construct( "DELETE", $table );
		}

		protected function buildQuery(): vector {
			$query = Vector{};
			$this->values = Vector{};

			// Statement
			$query->add( $this->statement );

			$query->add( "FROM" );

			// Table
			$query->add( $this->table );

			// Where
			$query->add( $this->where->query( $this->values ) );

			return $query;
		}
	}
