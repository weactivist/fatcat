<?hh // strict
	namespace core\db;

	use core\Exception;

	abstract class Statement {
		protected \PDO $pdo;
		protected int $limit = 0;
		protected int $offset = 0;
		protected string $statement = "";
		protected Vector $values = Vector{};
		protected string $table = "";
		protected Where $where;

		public function __construct( string $statement, string $table ) {
			$this->statement = $statement;
			$this->table = $table;
			$this->where = new Where();

			$connection = DbConnection::instance();
			$this->pdo = $connection->Resource();
		}

		public function debug(): void {
			print_r( $this->buildQuery() );
			var_dump( implode( " ", $this->buildQuery() ) );
			print_r( $this->values );
		}

		public function lastInsertId(): int {
			return intval( $this->pdo->lastInsertId() );
		}

		public function limit( int $limit ) {
			$this->limit = $limit;
		}

		public function offset( int $offset ) {
			$this->offset = $offset;
		}

		public function where( string $literal = "AND", string $field, string $operator, $value ) {
			$this->where->raw( $literal, $field, $operator, $value );
		}

		public function whereClause( Where $where ): void {
			$this->where = $where;
		}

		public function query(): \PDOStatement {
			$query = $this->buildQuery();

			$statement = $this->pdo->prepare( implode( " ", $query ) );

			try
			{
				$statement->execute( $this->values->toArray() );
			}
			catch( \PDOException $e )
			{
				Exception::ServiceUnavailable( $e->getMessage() );
			}

			Stats::instance()->addQuery( implode( " ", $query ), $this->values );

			return $statement;
		}

		abstract protected function buildQuery(): vector;
	}
