<?hh // strict
	namespace core\db;

	class Select extends Statement {
		private Set $fields = Set{};
		private Vector $join = Vector{};
		private Vector $orderBy = Vector{};

		public function __construct( string $table ) {
			parent::__construct( "SELECT", $table );
		}

		public function field( string $field ) {
			$this->fields->add( $field );
		}

		public function join( string $table, string $column1, string $column2 ) {
			$this->join->add( Vector{ $table, $column1, $column2 } );
		}

		public function orderBy( string $column, bool $desc = false ): void {
			$this->orderBy->add( Vector{ $column, $desc } );
		}

		public function exists(): bool {
			return (bool)$this->query()->fetch( \PDO::FETCH_ASSOC );
		}

		protected function buildQuery(): vector {
			$query = Vector{};
			$this->values = Vector{};

			// Statement
			$query->add( $this->statement );

			// Fields
			$query->add( $this->fields->isEmpty() ? "*" : implode( ", ", $this->fields ) );

			// Table
			$query->add( "FROM" );
			$query->add( $this->table );

			// Join
			if( !$this->join->isEmpty() ) {
				foreach( $this->join AS $key => $join ) {
					list( $table, $column1, $column2 ) = $join;

					if( $key > 0 ) {
						$query->add( "AND" );
					}

					$query->add( "JOIN" );
					$query->add( $table );
					$query->add( "ON" );
					$query->add( $column1 );
					$query->add( "=" );
					$query->add( $column2 );
				}
			}

			// Where
			$query->add( $this->where->query( $this->values ) );

			// Order By
			if( !$this->orderBy->isEmpty() ) {
				$query->add( "ORDER BY" );

				foreach( $this->orderBy AS $key => $orderBy ) {
					list( $field, $desc ) = $orderBy;

					if( $key > 0 ) {
						$query->add( "," );
					}

					$query->add( $field );
					$query->add( $desc ? "DESC" : "ASC" );
				}
			}

			if( $this->limit > 0 ) {
				$query->add( "LIMIT" );
				$query->add( $this->limit );
			}

			if( $this->offset > 0 ) {
				$query->add( "OFFSET" );
				$query->add( $this->offset );
			}

			return $query;
		}
	}