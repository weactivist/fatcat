<?hh // strict
	namespace core\db;

	class Where {
		protected vector $where = Vector{};

		public function andOperator( string $field, string $operator, $value ): void {
			$this->raw( "AND", $field, $operator, $value );
		}

		public function orOperator( string $field, string $operator, $value ): void {
			$this->raw( "OR", $field, $operator, $value );
		}

		public function raw( string $literal = "AND", string $field, string $operator, $value ): void {
			$this->where->add( Vector{ $literal, $field, $operator, $value } );
		}

		public function query( Vector &$values ): string {
			if( $this->where->isEmpty() ) {
				return "";
			}

			$query = Vector{};
			$query->add( "WHERE" );

			foreach( $this->where AS $key => $where ) {
				list( $literal, $field, $operator, $value ) = $where;

				$values->add( $value );

				if( $key > 0 )
					$query->add( $literal );

				$query->add( $field );
				$query->add( $operator );
				$query->add( "?" );
			}

			return implode( " ", $query );
		}
	}