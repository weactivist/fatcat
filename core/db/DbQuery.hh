<?hh // strict
	namespace core\db;

	class DbQuery
	{
		public static function Raw( string $queryString ): void
		{
			$connection = DbConnection::instance();
        	$pdo = $connection->Resource();
			$pdo->query( $queryString );
		}

		private Vector $where = Vector{};
		private \PDO $pdo;
		private string $method;
		private string $statement;
		private string $table;

		public function __construct( string $table, string $method ): void
		{
			$connection = new DbConnection();
			$this->pdo = $connection->Resource();

			$this->method = $method;
			$this->table = $table;
		}

		public function Debug(): void
		{
			$this->BuildQuery();
			echo $this->statement;
		}

		public function Query(): \PDOStatement
		{
			$this->BuildQuery();
			return $this->Execute();
		}

		public function Where( $arg1, string $operator, $arg2 ): DbQuery
		{
			$this->where->add( Vector{ $arg1, $operator, $arg2 } );
			return $this;
		}

		private function BuildQuery(): void
		{
			if( $this->statement )
				return;

			$this->statement = $this->method." * FROM ".$this->table." ";

			if( !$this->where->isEmpty() )
			{
				$this->statement .= "WHERE ";

				foreach( $this->where AS $where )
				{
					$this->statement .= $where->get( 0 )." ".$where->get( 1 )." ?";
				}
			}
		}

		private function Execute(): \PDOStatement
		{
			$pdo = $this->pdo->prepare( $this->statement );

			foreach( $this->where AS $where )
			{
				$pdo->execute( $where->get( 2 ) );
			}

			Stats::instance()->addQuery( $this->statement );

			return $pdo;
		}
	}
