<?hh // strict
	namespace core;

	abstract class Seeds {
		abstract public function Run(): void;
		abstract public function Destroy(): void;

		public static function name(): Str {
			 $string = new Str( Str::random( rand( 5, 15 ) ) );
			 $string->capitalize();
			 return $string;
		}

		private Vector $models = Vector{};

		protected function model( Model $model ) {
			$this->models->add( $model );
		}

		public function save(): void {
			foreach( $this->models AS $model ) {
				$vars = get_object_vars( $model );

				foreach( $vars AS $name => $value ) {
					if( $value instanceof Str ) {
						$model->$name = $value->get();
					}
				}

				$model->save();
			}
		}
	}