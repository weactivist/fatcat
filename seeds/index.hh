<?hh // strict
	namespace seeds;

	use \core\Exception;

	require_once( "../core/App.hh" );

	class Seed {
		private Vector $classes = Vector{};
		private Vector $files = Vector{};

		public function Run(): void {
			$this->Prepare();

			foreach( $this->classes AS $className ) {
				$class = new \ReflectionClass( $className );

				// Check if class is already seeded
				#if( self::Exists( $class->getFileName() ) )
				#	continue;

				// Validate the class
				if( !preg_match( "/Seeds/i", $class->getParentClass()->getName() ) ) {
					Exception::InternalServerError( "The class $className do not inherit from Migrations." );
				}

				$class = new $className;

				// Call the Run method
				$class->Run();

				// Call save method
				$class->save();

				// Update database with this row
				#self::Insert( $class->getFileName(), $className );

				#if( $callback != "" )
				#	$callback( $class->getFileName(), $className );
			}
		}

		public function Destroy(): void {
			$this->Prepare();

			foreach( $this->classes AS $className ) {
				$class = new \ReflectionClass( $className );

				// Check if class is already seeded
				#if( self::Exists( $class->getFileName() ) )
				#	continue;

				// Validate the class
				if( !preg_match( "/Seeds/i", $class->getParentClass()->getName() ) ) {
					Exception::InternalServerError( "The class $className do not inherit from Migrations." );
				}

				// Call the Run method
				$method = $class->getMethod( "Destroy" );
				$method->invoke( new $className );

				// Update database with this row
				#self::Insert( $class->getFileName(), $className );

				#if( $callback != "" )
				#	$callback( $class->getFileName(), $className );
			}
		}

		private function GetHandle(): resource {
			if( !( $handle = opendir( "./" ) ) ) {
				Exception::InternalServerError();
			}

			return $handle;
		}

		private function Prepare(): void {
			$this->PrepareFiles();
        	$this->LoadFiles();
        	$this->PrepareClasses();
		}

		private function PrepareClasses(): void {
			foreach( get_declared_classes() AS $className ) {
				if( $className === "seeds\Seed" ) {
					continue;
				}

				if( !preg_match( "/^seeds\\\/", $className ) ) {
					continue;
				}

				$this->classes->add( $className );
			}
		}

		private function PrepareFiles(): void {
			$handle = $this->GetHandle();

			while( $file = readdir( $handle ) ) {
				// Skip directories
				if( $file == "." || $file == ".." || $file == "index.hh" ) {
					continue;
				}

				// Check to see if this is a seeds file
				if( !preg_match( "/^[0-9]{6}_[0-9]{4}_[a-z]+\.hh$/i", $file ) ) {
					continue;
				}

				$this->files->add( $file );
			}
		}

		private function LoadFiles(): void {
			foreach( $this->files AS $file ) {
				require_once( $file );
			}
		}
	}

	$app = new \App();

	$args = new Vector( $argv );

	$seed = new Seed();

	switch( $args->get( 1 ) ) {
		case "r": $seed->Run(); break;
		case "d": $seed->Destroy(); break;
	}