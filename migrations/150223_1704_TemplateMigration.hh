<?hh //strict
	namespace migrations;

	use \core\Migrations;
	use \core\db\DbQuery;

	/**
	 * hhvm -f index.hh r
	 * hhvm -f index.hh d
	 */
	class TemplateMigration extends Migrations {
		public function Run(): void {
			DbQuery::Raw( "CREATE TABLE IF NOT EXISTS migrations
                           (
                               id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                               file VARCHAR(63) NOT NULL
                           );
                           ALTER TABLE gameofcards.migrations ADD CONSTRAINT unique_file UNIQUE (file);" );
		}

		public function Destroy(): void {
			DbQuery::Raw( "DROP TABLE IF EXISTS migrations;" );
		}
	}