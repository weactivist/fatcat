<?hh // strict
	namespace migrations;

	use \core\Exception;
	use \core\db\DbConnection;

	require_once( "../core/App.hh" );

	class Migrate {
		private static function CreateDatabase(): void {
			$config = new \Config();

			$connection = new DbConnection( "INFORMATION_SCHEMA" );
			$pdo = $connection->Resource();

			$statement = $pdo->prepare( "SELECT * FROM SCHEMATA WHERE SCHEMA_NAME = ?" );
			$statement->execute( array( $config->database->get( "database" ) ) );

			if( $statement->fetch() === false ) {
				$pdo->query( "CREATE DATABASE IF NOT EXISTS ".$config->database->get( "database" ) );
			}
		}

		private Vector $classes = Vector{};
		private Vector $files = Vector{};

		public function Run(): void {
			$this->Prepare();

			foreach( $this->classes AS $className ) {
				$class = new \ReflectionClass( $className );

				// Check if class is already migrated
				#if( self::Exists( $class->getFileName() ) )
				#	continue;

				// Validate the class
				if( !preg_match( "/Migrations/i", $class->getParentClass()->getName() ) ) {
					Exception::InternalServerError( "The class $className do not inherit from Migrations." );
				}

				// Call the Run method
				$method = $class->getMethod( "Run" );
				$method->invoke( new $className );

				// Update database with this row
				#self::Insert( $class->getFileName(), $className );

				#if( $callback != "" )
				#	$callback( $class->getFileName(), $className );
			}
		}

		public function Destroy(): void {
			$this->Prepare();

			foreach( $this->classes AS $className ) {
				$class = new \ReflectionClass( $className );

				// Check if class is already migrated
				#if( self::Exists( $class->getFileName() ) )
				#	continue;

				// Validate the class
				if( !preg_match( "/Migrations/i", $class->getParentClass()->getName() ) ) {
					Exception::InternalServerError( "The class $className do not inherit from Migrations." );
				}

				// Call the Run method
				$method = $class->getMethod( "Destroy" );
				$method->invoke( new $className );

				// Update database with this row
				#self::Insert( $class->getFileName(), $className );

				#if( $callback != "" )
				#	$callback( $class->getFileName(), $className );
			}
		}

		private function GetHandle(): resource {
			if( !( $handle = opendir( "./" ) ) ) {
				Exception::InternalServerError();
			}

			return $handle;
		}

		private function Prepare(): void {
			self::CreateDatabase();

			$this->PrepareFiles();
        	$this->LoadFiles();
        	$this->PrepareClasses();
		}

		private function PrepareClasses(): void {
			foreach( get_declared_classes() AS $className ) {
				if( $className === "migrations\Migrate" ) {
					continue;
				}

				if( !preg_match( "/^migrations\\\/", $className ) ) {
					continue;
				}

				$this->classes->add( $className );
			}
		}

		private function PrepareFiles(): void {
			$handle = $this->GetHandle();

			while( $file = readdir( $handle ) ) {
				// Skip directories
				if( $file == "." || $file == ".." || $file == "index.hh" ) {
					continue;
				}

				// Check to see if this is a migrations file
				if( !preg_match( "/^[0-9]{6}_[0-9]{4}_[a-z]+\.hh$/i", $file ) ) {
					continue;
				}

				$this->files->add( $file );
			}
		}

		private function LoadFiles(): void {
			foreach( $this->files AS $file ) {
				require_once( $file );
			}
		}
	}

	$app = new \App();

	$args = new Vector( $argv );

	$migrate = new Migrate();

	switch( $args->get( 1 ) ) {
		case "r": $migrate->Run(); break;
		case "d": $migrate->Destroy(); break;
	}